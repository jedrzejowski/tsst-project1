using System;
using System.IO;
using System.Text;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace ClientNode {
    class Program {
        static void Main(string[] args) {
            if (args.Length != 1)
                return;

            var path = args[0];
            var settings = JObject.Parse(File.ReadAllText(path));

            var clientNode = new ClientNode(settings);

            TSST.ConsoleUI.init();
        }
    }

    class ClientNode {

        TSST.Port port;

        public ClientNode(JObject settings) {
            readConfig(settings);

            initPort();

            TSST.ConsoleUI.CommandEvent += new TSST.ConsoleUI.OnCommand(cmd => this.cmd(cmd));
        }

        void readConfig(JObject settings) {
            TSST.Device.Name = settings["name"]?.ToObject<string>() ?? TSST.Device.Name;
        }

        void initPort() {
            port = TSST.Port.toCableCloud(TSST.Device.Name);

            port.msgEvent += new TSST.OnPackage((pkg) => {
                TSST.ConsoleUI.log($"Received {pkg}{Environment.NewLine}{pkg.bodyToString()}");
            });
        }

        void cmd(string[] cmd) {
            try {
                switch (cmd[0]) {
                    case "sendmsg":
                        cmd_sendMsg(cmd[1], cmd[2]);
                        break;

                    default:
                        TSST.ConsoleUI.err($"Command '{cmd[0]}' not found");
                        break;
                }
            } catch (Exception e) {
                TSST.ConsoleUI.err(e.ToString());
            }
        }

        void cmd_sendMsg(string header, string body) {
            var pkg = new TSST.Package();
            pkg.label = UInt32.Parse(header);
            pkg.body = Encoding.UTF8.GetBytes(body);
            port.send(pkg);
            
            TSST.ConsoleUI.log($"{pkg} was sended");
        }
    }
}