﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace ManagerNode {
    public delegate void Callback(string message);

    class Program {

        static void Main(string[] args) {
            if (args.Length != 1)
                return;

            var path = args[0];
            var settings = JObject.Parse(File.ReadAllText(path));

            var managerNode = new ManagerNode(settings);

            TSST.ConsoleUI.init();
        }
    }

    class ManagerNode {

        Dictionary<string, TSST.Port> ports = new Dictionary<string, TSST.Port>();

        public ManagerNode(JObject settings) {
            readConfig(settings);

            TSST.ConsoleUI.CommandEvent += new TSST.ConsoleUI.OnCommand(cmd => this.cmd(cmd));
        }

        void readConfig(JObject settings) {
            TSST.Device.Name = settings["name"]?.ToObject<string>();

            foreach (JObject portDef in settings["ports"] as JArray)
                initPort(portDef);

        }

        async void initPort(JObject portDef) {

            var portName = portDef["name"].ToString();

            var port = TSST.Port.toCableCloud($"{TSST.Device.Name}:{portName}");

            lock(ports) ports.Add(portName, port);

            port.msgEvent += new TSST.OnPackage(pkg => {

            });
        }

        #region Remote Getters

        TSST.Link[] getLinks(){
            return null;
        }

        TSST.RedirectRule getRedirectRules(string name){
            return null;
        }

        #endregion

        #region CMD

        void cmd(string[] cmd) {
            try {

                switch (cmd[0]) {
                    case "sendcmd":
                        var args = new string[cmd.Length - 2];
                        Array.Copy(cmd, 2, args, 0, cmd.Length - 2);
                        cmd_sendcmd(cmd[1], string.Join(" ", args));
                        break;

                    default:
                        TSST.ConsoleUI.err($"Command '{cmd[0]}' not found");
                        break;
                }
            } catch (Exception e) {
                TSST.ConsoleUI.err(e.ToString());
            }
        }

        void cmd_sendcmd(string to, string cmd) {
            var pkg = new TSST.Package();
            pkg.bodyFromString(cmd);
            
            ports[to].send(pkg);
        }

        #endregion
    }
}