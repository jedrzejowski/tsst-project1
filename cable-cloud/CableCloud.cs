﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CableCloud {

    class Program {
        static void Main(string[] args) {
            if (args.Length != 1) {
                Console.WriteLine("Require config file as argument");
                return;
            }

            var path = args[0];
            var settings = JObject.Parse(File.ReadAllText(path));

            var cableCloud = new CableCloud(settings);
            cableCloud.run();
        }
    }

    class CableCloud {

        int port = 5000;
        TcpListener tcpServer;
        Dictionary<string, TSST.Port> ports = new Dictionary<string, TSST.Port>();
        List<TSST.Link> links = new List<TSST.Link>();

        public CableCloud(JObject settings) {
            readConfig(settings);

            TSST.ConsoleUI.CommandEvent += new TSST.ConsoleUI.OnCommand(cmd => this.cmd(cmd));
        }

        void readConfig(JObject settings) {
            TSST.Device.Name = settings["name"]?.ToObject<string>();
            port = settings["tcpPort"]?.ToObject<int>() ?? port;

            foreach (JObject ruleDef in settings["links"] as JArray) {
                addLink(ruleDef);
            }
        }

        void addLink(JObject ruleDef) {

            lock(links) {
                links.Add(new TSST.Link(ruleDef));
            }
        }

        public void run() {
            new Thread(() => {

                tcpServer = new TcpListener(IPAddress.Any, port);

                tcpServer.Start();

                while (true) {
                    TcpClient client = tcpServer.AcceptTcpClient();
                    new Thread(() => initPort(client)).Start();
                }

                tcpServer.Stop();

            }).Start();

            TSST.ConsoleUI.init();
        }

        void initPort(TcpClient client) {

            var port = new TSST.Port(client);

            bool firstMsg = true;

            port.msgEvent += new TSST.OnPackage((pkg) => {

                if (firstMsg) {
                    firstMsg = false;
                    port.name = Encoding.UTF8.GetString(pkg.body);
                    TSST.ConsoleUI.log($"{port} connected");

                    lock(ports) ports.Add(port.name, port);

                } else {

                    var log = $"{port} received {pkg}";

                    foreach (var link in links) {
                        if (link.port1 == port.name) {
                            Thread.Sleep(link.delay);

                            if (!ports.ContainsKey(link.port2)) break;
                            var targetPort = ports[link.port2];
                            targetPort.send(pkg);

                            log += $" sended to {targetPort}";
                            TSST.ConsoleUI.log(log);
                            return;
                        }
                    }

                    log += $", was lost";
                    TSST.ConsoleUI.log(log);
                }
            });

            port.disconnectEvent += new TSST.On(() => {
                TSST.ConsoleUI.log($"{port} disconnected");

                if (!firstMsg)
                    lock(ports) {
                        ports.Remove(port.name);
                    }
            });

            port.beginRead();
        }

        #region CMD

        void cmd(string[] cmd) {
            try {

                switch (cmd[0]) {
                    case "del":
                        cmd_del(cmd[1], cmd[2]);
                        break;

                    case "add":
                        if (cmd.Length == 3)
                            cmd_add(cmd[1], cmd[2]);

                        if (cmd.Length == 5)
                            cmd_add(cmd[1], cmd[2], int.Parse(cmd[3]), int.Parse(cmd[4]));

                        break;

                    case "list":
                        cmd_list();
                        break;

                    default:
                        TSST.ConsoleUI.err($"Command '{cmd[0]}' not found");
                        break;
                }
            } catch (Exception e) {
                TSST.ConsoleUI.err(e.ToString());
            }
        }

        void cmd_add(string port1, string port2) {
            lock(links) {
                links.Add(new TSST.Link(port1, port2));
            }
        }

        void cmd_add(string port1, string port2, int delay, int weight) {
            lock(links) {
                links.Add(new TSST.Link(port1, port2, delay, weight));
            }
        }

        void cmd_del(string port1, string port2) {
            foreach (var link in links) {
                if (link.port1 == port1 && link.port2 == port2) {
                    lock(links) links.Remove(link);
                    TSST.ConsoleUI.log($"{link} deleted");
                    return;
                }
            }

            TSST.ConsoleUI.log($"No link was deleted");
        }

        void cmd_list() {
            var output = $"Listing links";
            foreach (var link in links) {
                output += $"{Environment.NewLine}{link}";
            }
            TSST.ConsoleUI.log(output);
        }
        
        #endregion

    }

}