using System;
using System.Collections.Generic;

//https://github.com/mburst/dijkstras-algorithm/blob/master/dijkstras.cs

namespace TSST {
    class Graph {

        Dictionary<string, Dictionary<string, int>> vertices = new Dictionary<string, Dictionary<string, int>>();

        public void addVertex(string name, Dictionary<string, int> edges) {
            vertices[name] = edges;
        }

        public List<string> shortestPath(string start, string finish) {
            var previous = new Dictionary<string, string>();
            var distances = new Dictionary<string, int>();
            var nodes = new List<string>();

            List<string> path = null;

            foreach (var vertex in vertices) {
                if (vertex.Key == start) {
                    distances[vertex.Key] = 0;
                } else {
                    distances[vertex.Key] = int.MaxValue;
                }

                nodes.Add(vertex.Key);
            }

            while (nodes.Count != 0) {
                nodes.Sort((x, y) => distances[x] - distances[y]);

                var smallest = nodes[0];
                nodes.Remove(smallest);

                if (smallest == finish) {
                    path = new List<string>();
                    while (previous.ContainsKey(smallest)) {
                        path.Add(smallest);
                        smallest = previous[smallest];
                    }

                    break;
                }

                if (distances[smallest] == int.MaxValue) {
                    break;
                }

                foreach (var neighbor in vertices[smallest]) {
                    var alt = distances[smallest] + neighbor.Value;
                    if (alt < distances[neighbor.Key]) {
                        distances[neighbor.Key] = alt;
                        previous[neighbor.Key] = smallest;
                    }
                }
            }

            return path;
        }

        
    }

    // class MainClass {
    //     public static void Main(string[] args) {
    //         Graph g = new Graph();
    //         g.add_vertex('A', new Dictionary<string, int>() { { 'B', 7 }, { 'C', 8 } });
    //         g.add_vertex('B', new Dictionary<string, int>() { { 'A', 7 }, { 'F', 2 } });
    //         g.add_vertex('C', new Dictionary<string, int>() { { 'A', 8 }, { 'F', 6 }, { 'G', 4 } });
    //         g.add_vertex('D', new Dictionary<string, int>() { { 'F', 8 } });
    //         g.add_vertex('E', new Dictionary<string, int>() { { 'H', 1 } });
    //         g.add_vertex('F', new Dictionary<string, int>() { { 'B', 2 }, { 'C', 6 }, { 'D', 8 }, { 'G', 9 }, { 'H', 3 } });
    //         g.add_vertex('G', new Dictionary<string, int>() { { 'C', 4 }, { 'F', 9 } });
    //         g.add_vertex('H', new Dictionary<string, int>() { { 'E', 1 }, { 'F', 3 } });

    //         g.shortest_path('A', 'H').ForEach(x => Console.WriteLine(x));
    //     }
    // }
}