using System;
using System.Text;
using System.Threading;

namespace TSST {

    public class ConsoleUI {

        public delegate void OnCommand(string[] cmd);
        public static event OnCommand CommandEvent;

        public static void init() {
            Console.Clear();

            new Thread(() => {

                while (true) {
                    var cmd = readCommand();

                    if (cmd[0].Length == 0)
                        continue;

                    if (CommandEvent != null)
                        CommandEvent.Invoke(cmd);
                }

            }).Start();
        }

        static String time(){
            // return DateTime.Now.ToString("yyyy:MM:dd:HH:mm:ss:ffffff");
            return DateTime.Now.ToString("⌚ HH:mm:ss");
        }

        static void clearLine() {
            Console.Write("\r" + new string(' ', Console.BufferWidth) + "\r");
        }

        static void writeLine(string line) {
            writeLine(line, "\x1b[0;0m");
        }

        static void writeLine(string line, string colorASCII) {
            lock(currentCmd) {
                clearLine();
                Console.WriteLine($"{colorASCII}{line}");
                Console.ResetColor();
                renderCommand();
            }
        }

        static void renderCommand() {
            // Console.Write("\x33[1D");

            clearLine();
            Console.Write($"{Device.Name}$> {currentCmd}");
        }

        public static void log(object msg) {
            writeLine($"{Device.Name} {time()}> {msg}");
        }

        public static void err(object msg) {
            writeLine($"{Device.Name} {time()}> {msg}", "\x1b[91m");
        }

        public static void warn(string msg) {
            writeLine($"{Device.Name} {time()}> {msg}", "\x1b[93m");
        }

        static string currentCmd = "";
        static bool commandReading = false;

        static string[] readCommand() {

            commandReading = true;

            while (true) {
                renderCommand();
                var key = Console.ReadKey();

                lock(currentCmd) {

                    if (key.Key == ConsoleKey.Enter) {
                        Console.Write(Environment.NewLine);
                        var ret = currentCmd.Split(' ');
                        currentCmd = "";
                        commandReading = false;
                        return ret;
                    }

                    if (key.Key == ConsoleKey.Backspace) {
                        if (currentCmd.Length == 0) continue;

                        currentCmd = currentCmd.Substring(0, currentCmd.Length - 1);

                        renderCommand();

                        continue;
                    }

                    currentCmd += key.KeyChar;
                }
            }
        }
    }
}