using System;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace TSST {
    public class Package {
        public byte[] body;
        public uint label;

        public Package() {
            label = 0;
            body = new byte[0];
        }

        public Package(byte[] bytes) {

            var h = new byte[4];
            Array.Copy(bytes, 0, h, 0, 4);
            label = BitConverter.ToUInt32(h, 0);

            body = new byte[bytes.Length - 4];
            Array.Copy(bytes, 4, body, 0, bytes.Length - 4);
        }

        public Package(uint label, byte[] body) {
            this.label = label;
            this.body = body;
        }

        public Package(uint label, Package body) {
            this.label = label;
            this.body = body.fullBytes();
        }

        public byte[] fullBytes() {
            var ret = new byte[body.Length + 4];

            Array.Copy(BitConverter.GetBytes(label), 0, ret, 0, 4);
            Array.Copy(body, 0, ret, 4, body.Length);

            return ret;
        }

        public string bodyToString() {
            return Encoding.UTF8.GetString(body);
        }

        public void bodyFromString(string str) {
            body = Encoding.UTF8.GetBytes(str);
        }


        public override string ToString() {
            return $"Pakage({label}, {body.Length})";
        }
    }
}