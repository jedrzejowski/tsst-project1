using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace TSST {
    public class ClientRule {
        public uint ip, label;

        public ClientRule() {

        }

        public ClientRule(JObject ruleDef) {
            ip = ruleDef["ip"].ToObject<uint>();
            label = ruleDef["label"].ToObject<uint>();
        }

        public override string ToString() {
            return $"ClientRule({ip}:{label})";
        }
    }
}