using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace TSST {
    public class RedirectRule {
        public int stack;
        public uint label1, label2;
        public string port1, port2;

        public RedirectRule() {

        }

        public RedirectRule(JObject ruleDef) {
            stack = ruleDef["stack"]?.ToObject<int>() ?? 0;
            label1 = ruleDef["label1"]?.ToObject<uint>() ?? 0;
            label2 = ruleDef["label2"]?.ToObject<uint>() ?? 0;
            port1 = ruleDef["port1"].ToString();
            port2 = ruleDef["port2"].ToString();
        }

        public override string ToString() {
            return $"RedirectRule({port1}:{label1}->{stack}->{port2}:${label2})";
        }
    }
}