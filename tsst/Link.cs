using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace TSST {
    public class Link {
        public string port1;
        public string port2;
        public int delay = 1000;
        public int weight = 1000;

        public Link(JObject def) {
            port1 = def["port1"].ToString();
            port2 = def["port2"].ToString();

            delay = def["delay"]?.ToObject<int>() ?? delay;
            weight = def["weight"]?.ToObject<int>() ?? weight;
        }

        public Link(string port1, string port2) {
            this.port1 = port1;
            this.port2 = port2;
        }

        public Link(string port1, string port2, int delay, int weight) {
            this.port1 = port1;
            this.port2 = port2;
            this.delay = delay;
            this.weight = weight;
        }

        public override string ToString() {
            return $"Link({port1}->{port2},{delay},{weight})";
        }

        JObject toDef(){
            var def = new JObject();

            def["port1"] = port1;
            def["port2"] = port2;
            def["delay"] = delay;
            def["weight"] = weight;
            
            return def;
        }
    }
}