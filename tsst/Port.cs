using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace TSST {
    public delegate void OnPackage(Package msg);
    public delegate void On();

    public class Port {

        // static Dictionary<string, TSST.Socket> All = new Dictionary<string, TSST.Socket>();

        // public static Socket Get(string name) {
        // return All[name];
        // }

        TcpClient tcpClient;
        NetworkStream netStream;
        public string name = "Unnamed";
        Thread readingThread;

        public event OnPackage msgEvent;
        public event On disconnectEvent;

        public Port(string name, TcpClient tcpClient) {
            this.name = name;
            this.tcpClient = tcpClient;
            netStream = tcpClient.GetStream();
        }

        public Port(TcpClient tcpClient) : this("", tcpClient) { }

        public static Port toCableCloud(string name) {
            var tcpClient = new TcpClient();
            tcpClient.Connect("localhost", 5000);

            var socket = new Port(name, tcpClient);
            socket.sendAuth();
            socket.beginRead();

            return socket;
        }

        public void beginRead() {
            if (readingThread != null) return;

            readingThread = new Thread(() => {
                while (tcpClient.Connected)
                    readMsg();

            });

            readingThread.Start();
        }

        public void disconnect() {
            tcpClient.Close();

            if (disconnectEvent != null)
                disconnectEvent.Invoke();
        }

        public void send(Package msg) {
            if (!tcpClient.Connected) return;
            
            lock(netStream) {
                var msgByte = msg.fullBytes();

                var lengthByte = BitConverter.GetBytes(msgByte.Length);

                netStream.Write(lengthByte, 0, lengthByte.Length);
                netStream.Write(msgByte, 0, msgByte.Length);
            }
        }

        private void readMsg() {
            byte[] bytes = new byte[4];
            uint length;

            if (netStream.Read(bytes, 0, bytes.Length) == 0) {
                disconnect();
                return;
            }

            length = BitConverter.ToUInt32(bytes, 0);
            bytes = new byte[length];

            if (length > 0 && netStream.Read(bytes, 0, bytes.Length) == 0) {
                disconnect();
                return;
            }

            var msg = new Package(bytes);
            msgEvent.Invoke(msg);
        }

        private void sendAuth() {
            var msg = new Package();
            msg.body = Encoding.UTF8.GetBytes(name);
            send(msg);
        }

        public override string ToString() {
            return $"Port({name})";
        }
    }
}