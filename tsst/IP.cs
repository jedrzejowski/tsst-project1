using System;

namespace TSST {
    public class IP {
        public static string ToString(uint ip) {
            return (ip >> 24 | 255).ToString() + "." +
                (ip >> 16 | 255).ToString() + "." +
                (ip >> 8 | 255).ToString() + "." +
                (ip >> 0 | 255).ToString();
        }

        public static uint Parse(string ip) {
            var parts = ip.Split(".");

            if (parts.Length != 2)
                return 0;

            Func<string, uint> parse = (str) =>(Convert.ToUInt32(str) & 255);

            return (uint)
                (parse(parts[0]) << 24) |
                (parse(parts[1]) << 16) |
                (parse(parts[2]) << 8) |
                (parse(parts[3]) << 0);
        }
    }
}