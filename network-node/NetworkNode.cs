﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace NetworkNode {
    class Program {

        static void Main(string[] args) {
            if (args.Length != 1)
                return;

            var path = args[0];
            var settings = JObject.Parse(File.ReadAllText(path));

            var networkNode = new NetworkNode(settings);

            TSST.ConsoleUI.init();
        }
    }

    class NetworkNode {

        Dictionary<string, TSST.Port> ports = new Dictionary<string, TSST.Port>();
        List<TSST.RedirectRule> redirectRules = new List<TSST.RedirectRule>();
        List<TSST.ClientRule> clientRules = new List<TSST.ClientRule>();

        TSST.Port controlPort;

        public NetworkNode(JObject settings) {
            readConfig(settings);

            initControl();

            TSST.ConsoleUI.CommandEvent += new TSST.ConsoleUI.OnCommand(cmd => {
                try {
                    TSST.ConsoleUI.log(this.cmd(cmd));
                } catch (Exception e) {
                    TSST.ConsoleUI.err(e.ToString());
                }
            });
        }

        void readConfig(JObject settings) {
            TSST.Device.Name = settings["name"]?.ToObject<string>();

            foreach (JObject rule in settings["redirectRules"] as JArray)
                redirectRules.Add(new TSST.RedirectRule(rule));

            foreach (JObject rule in settings["clientRules"] as JArray)
                clientRules.Add(new TSST.ClientRule(rule));

            foreach (JObject portDef in settings["ports"] as JArray)
                initPort(portDef);

        }

        async void initPort(JObject portDef) {

            var portName = portDef["name"].ToString();

            var isClient = portDef["client"]?.ToObject<bool>() ?? false;
            var clientIP = portDef["clientIP"]?.ToObject<uint>() ?? 0;

            var port = TSST.Port.toCableCloud($"{TSST.Device.Name}:{portName}");

            lock(ports) ports.Add(portName, port);

            port.msgEvent += new TSST.OnPackage((pkg) => {

                var log = $"{port} received {pkg}";
                var lost = true;

                if (isClient) {
                    foreach (var rule in clientRules)
                        if (rule.ip == pkg.label) {

                            pkg.label = clientIP;

                            pkg = new TSST.Package(rule.label, pkg);

                            log += $"(from client), label added {pkg}";

                            break;
                        }
                }

                foreach (var rule in redirectRules)
                    if (rule.port1 == portName && rule.label1 == pkg.label) {

                        switch (rule.stack) {
                            case 0:
                                pkg.label = rule.label2;
                                log += $", label changed {pkg}";
                                break;

                            case 1:
                                pkg = new TSST.Package(rule.label2, pkg);
                                log += $", label added {pkg}";
                                break;

                            case -1:
                                pkg = new TSST.Package(pkg.body);
                                log += $", label removed {pkg}";
                                break;
                        }

                        var targetPort = ports[rule.port2];
                        ports[rule.port2].send(pkg);

                        log += $", sended to {targetPort}";
                        lost = false;
                        break;
                    }

                if (lost) {
                    log += $", lost";
                    TSST.ConsoleUI.warn(log);
                } else {
                    TSST.ConsoleUI.log(log);
                }
            });
        }

        async void initControl() {
            controlPort = TSST.Port.toCableCloud($"{TSST.Device.Name}:control");

            controlPort.msgEvent += new TSST.OnPackage((pkg) => {
                var cmd = pkg.bodyToString();
                TSST.ConsoleUI.log($"Received remote command `{cmd}`");

                try {
                    var output = this.cmd(cmd.Split(' '));

                } catch (Exception e) {
                    TSST.ConsoleUI.err(e.ToString());
                }
            });
        }

        #region CMD

        string cmd(string[] cmd) {

            switch (cmd[0]) {
                case "rrls":
                    return cmd_rrls();

                case "crls":
                    return cmd_crls();

                case "rradd":
                    return cmd_rradd(cmd[1], cmd[2], cmd[3], cmd[4], cmd[4]);

                case "cradd":
                    return cmd_cradd(cmd[1], cmd[2]);

                case "rrdel":
                    return cmd_rrdel(cmd[1], cmd[2]);

                case "crdel":
                    return cmd_crdel(cmd[1]);

                default:
                    return $"Command '{cmd[0]}' not found";
            }
        }

        string cmd_rrls() {

            Func<string, int, string> toLeft = (str, size) => str + new string(' ', size - str.Length);

            string full = "RedirectRules" + Environment.NewLine;

            full += "| ";
            full += toLeft("port1", 7) + " | ";
            full += toLeft("label1", 6) + " | ";
            full += toLeft("label2", 6) + " | ";
            full += toLeft("port2", 7) + " | ";
            full += toLeft("stack", 6) + " | ";
            full += Environment.NewLine;

            foreach (var rule in redirectRules) {

                full += "| ";
                full += toLeft(rule.port1, 7) + " | ";
                full += toLeft(rule.label1.ToString(), 6) + " | ";
                full += toLeft(rule.label2.ToString(), 6) + " | ";
                full += toLeft(rule.port2, 7) + " | ";
                full += toLeft(rule.stack.ToString(), 6) + " | ";

                full += Environment.NewLine;
            }

            return full;
        }

        string cmd_crls() {

            Func<string, int, string> toLeft = (str, size) => str + new string(' ', size - str.Length);
            var full = "ClientRule" + Environment.NewLine;

            full += "| ";
            full += toLeft("ip", 7) + " | ";
            full += toLeft("label", 7) + " |";
            full += Environment.NewLine;

            foreach (var rule in clientRules) {

                full += "| ";
                full += toLeft(rule.ip.ToString(), 7) + " | ";
                full += toLeft(rule.label.ToString(), 7) + " |";

                full += Environment.NewLine;
            }

            return full;
        }

        string cmd_rradd(string port1, string label1, string label2, string port2, string stack) {
            var rr = new TSST.RedirectRule();
            rr.port1 = port1;
            rr.label1 = UInt32.Parse(label1);
            rr.label2 = UInt32.Parse(label2);
            rr.port2 = port2;
            rr.stack = Int32.Parse(stack);

            lock(redirectRules) redirectRules.Add(rr);
            return $"{rr} added";
        }

        string cmd_cradd(string ip, string label) {
            var cr = new TSST.ClientRule();
            cr.ip = UInt32.Parse(ip);
            cr.label = UInt32.Parse(label);

            lock(clientRules) clientRules.Add(cr);
            return $"{cr} added";
        }

        string cmd_rrdel(string port1, string label1) {
            uint h = UInt32.Parse(label1);

            foreach (var rule in redirectRules)
                if (rule.port1 == port1 && rule.label1 == h) {
                    lock(redirectRules) redirectRules.Remove(rule);
                    return $"{rule} deleted";
                }

            return "No rule was deleted";
        }

        string cmd_crdel(string ip) {
            uint i = UInt32.Parse(ip);

            foreach (var rule in clientRules)
                if (rule.ip == i) {
                    lock(clientRules) clientRules.Remove(rule);
                    return $"{rule} deleted";
                }
            return "No rule was deleted";
        }

        #endregion
    }

}